
#include <GL/glew.h>
//#include "SOIL.h"
//shaders
#include "FragmentShaders.h"
#include "VertexShaders.h"
//input/output
#include <iostream>
//glut stuff

#include <GL/freeglut.h>
//maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//texture loader
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

GLuint window;
GLdouble width = 800, height = 600;

glm::vec3 eye = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 target = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

std::string tex_path = "..\\Textures\\container.jpg";
//===============================================
// GLUT 
//===============================================

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myIdle();

//===============================================
// Function prototypes
//===============================================

void checkShaderCompileStatus(unsigned int ID);
void checkShaderLinkStatus(unsigned int ID);
bool stbi_loadTexture(unsigned int tID,
	std::string &filename,
	unsigned char*& buffer,
	int* width,
	int* height,
	int* nrChannels);
//===============================================
// global data
//===============================================
unsigned char* texture_data = NULL; 
int tex_width;
int tex_height;
int nchannels; 
unsigned int texture; 
unsigned int VBO, VAO, EBO, FBO;
//shaders
unsigned int vertexShader;
unsigned int fragmentShader;
unsigned int ourShader;

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3); //support OpenGL (>=3.2)
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);

	window = glutCreateWindow("Window");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	
	glewExperimental = true; 
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	float vertices[] = {
		// positions          // colors           // texture coords
		 0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
		 0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
		-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
	};
	float texCoords[] = {
	0.0f, 0.0f,  // lower-left corner  
	1.0f, 0.0f,  // lower-right corner
	0.5f, 1.0f   // top-center corner
	};

	unsigned int indices[] = {
	   0, 1, 3, // first triangle
	   1, 2, 3  // second triangle
	};


	//VAO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Vertex Attributes 
	glEnableVertexAttribArray(0); //position
	glVertexAttribPointer(
		0, 
		3, 
		GL_FLOAT, 
		GL_FALSE, 
		8 * sizeof(GLfloat), 
		(void*)(0)
	);
	glEnableVertexAttribArray(1); //color
	glVertexAttribPointer(1,
		3, 
		GL_FLOAT, 
		GL_FALSE, 
		8 * sizeof(GLfloat), 
		(void*)(3 * sizeof(GLfloat))
	);
	glEnableVertexAttribArray(2); //texture
	glVertexAttribPointer(2, 
		2, 
		GL_FLOAT, 
		GL_FALSE, 
		8 * sizeof(GLfloat), 
		(void*)(6 * sizeof(GLfloat))
	);

	//ucitaj teksturu
	glGenTextures(1, &texture);
	stbi_loadTexture(texture, tex_path, texture_data, &tex_width, &tex_height, &nchannels);
	

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	checkShaderCompileStatus(vertexShader);
	
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	checkShaderCompileStatus(fragmentShader);

	ourShader = glCreateProgram();
	glAttachShader(ourShader, vertexShader);
	glAttachShader(ourShader, fragmentShader);
	glLinkProgram(ourShader);
	checkShaderCompileStatus(ourShader);
	checkShaderLinkStatus(ourShader);
	
	glUseProgram(ourShader);
	glUniform1i(glGetUniformLocation(ourShader, "ourTexture"), 0);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glutMainLoop();
}

void myDisplay(void)
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glm::mat4 proj = glm::perspective<GLfloat>(
		glm::radians(45.0f),
		width / height,
		0.1f,
		100.0f
		);
	glm::mat4 view = glm::lookAt(eye, target, up);
	glm::mat4 model = glm::mat4(1.0f);

	glBindVertexArray(VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glUseProgram(ourShader);
	GLint proj_loc = glGetUniformLocation(ourShader, "u_proj");
	GLint view_loc = glGetUniformLocation(ourShader, "u_view");
	GLint model_loc = glGetUniformLocation(ourShader, "u_model");
	GLint tex_uniform0 = glGetUniformLocation(ourShader, "ourTexture");

	glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(model));
	glUniform1i(tex_uniform0, 0);

	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glutSwapBuffers();      
}


void myReshape(int w, int h)
{
	width = glutGet(GLUT_WINDOW_WIDTH);
	height = glutGet(GLUT_WINDOW_HEIGHT);
	
	glutPostRedisplay();
}

bool stbi_loadTexture(unsigned int tID, 
	std::string &filename,
	unsigned char*& buffer, 
	int* t_Width, 
	int* t_Height, 
	int* nrChannels) 
{
	if (FILE *file = fopen(filename.c_str(), "r")) {
		fclose(file);
		std::cout << "File exists" << std::endl;
	}
	else {
		std::cout << "File doesn't exist" << std::endl;
		return false;
	}
	
	buffer = stbi_load(filename.c_str(), t_Width, t_Height, nrChannels, 0);

	if (buffer)
	{
		glActiveTexture(GL_TEXTURE + tID);
		glBindTexture(GL_TEXTURE_2D, tID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)*t_Width, (GLsizei)*t_Height, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
		stbi_image_free(buffer);

		std::cout << "::STBI::SUCCESS::LOADED TEXTURE" << std::endl;
		std::cout << "width: " << t_Width << std::endl;
		std::cout << "height: " << t_Height << std::endl;

		return true;
	}
	else
	{
		std::cout << "::STBI::ERROR::STBI_LOAD RETURNED NULL" << std::endl;
	}


	return false;
}

void checkShaderLinkStatus(unsigned int ID) {
	int status; 
	GLchar infoLog[1024];
	glGetProgramiv(ID, GL_LINK_STATUS, &status);
	if (!status) {
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::" << ID <<"::LINKING_FAILURE\n" << infoLog << std::endl;
	}
	else {
		std::cout << "SHADER::" << ID << "::LINKING_SUCCESS" << std::endl;
	}
}

void checkShaderCompileStatus(unsigned int ID) {
	int status;
	GLchar infolog[1024];
	glGetShaderiv(ID, GL_COMPILE_STATUS, &status);
	if (!status) {
		glGetProgramInfoLog(ID, 512, NULL, infolog);
		std::cout << "ERROR::SHADER::" << ID << "::COMPILATION_FAILURE\n" << infolog << std::endl;
	}
	else {
		std::cout << "SHADER::" << ID << "::COMPILATION_SUCCESS" << std::endl;
	}
}