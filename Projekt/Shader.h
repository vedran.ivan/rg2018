#pragma once
#ifndef SHADER_H
#define SHADER_H 

#include <GL/glew.h>
//#include <GL/freeglut.h>


#include <iostream>

class Shader 
{
public:
	unsigned int ID; 

	Shader(const char* vertexShader, const char* fragmentShader);
	~Shader() {
		glDeleteShader(vertexShaderID);
		glDeleteShader(fragmentShaderID);
	}

	void compileShaders(); 
	void linkShaders(); 
	void loadShaders(); 

private: 
	unsigned int vertexShaderID; 
	unsigned int fragmentShaderID;
};

#endif // !SHADER_H
