#pragma once

const char* vertexShaderSource =
"#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"layout(location = 1) in vec3 aColor;\n"
"layout(location = 2) in vec2 aTexCoord;\n"
"out vec3 ourColor;\n"
"out vec2 TexCoord;\n"
"uniform mat4 u_proj;\n"
"uniform mat4 u_view;\n"
"uniform mat4 u_model;\n"
"void main()\n"
"{\n"
"gl_Position = u_proj * u_view * u_model * vec4(aPos, 1.0);\n"
"ourColor = aColor;\n"
"TexCoord = aTexCoord;\n"
"}\n";