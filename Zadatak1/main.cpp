
#ifdef _WIN32
#include <windows.h>             //bit ce ukljuceno ako se koriste windows
#include <Windef.h>
#endif

#include <stdio.h>
#include <GL/glut.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <glm/gtc/type_ptr.hpp>
#include "obj_loader.h"



#define N_PI 3.14159265358979323846

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

GLuint window;
GLuint width = 800, height = 600;

typedef struct {
	GLdouble	x;
	GLdouble	y;
	GLdouble	z;
}Tocka3D;


Tocka3D ociste = { 0, 0, 1.0 };
Tocka3D glediste = { 0.0, 0.0, 0.0 };

float angle; 
float oldMouseX;
float oldMouseY;

DrawableObject* obj_to_draw;

char* model_path = "C:/Projects/IRG/IRGLabosi/zad1/models/f16.obj";
char* spline_path = "C:/Projects/IRG/IRGLabosi/zad1/bspline_runway.txt";

int brB = 0; //broj tocaka za stvaranje splinea
int brS = 0; //broj segmenata

glm::vec3 p_acc(0.0f); //akumulirana pozicija tocke od svih segmenata

std::vector<glm::vec3> spline_tocke;
std::vector<glm::vec3> splineB; // p(t) tocke bspline-a za mjenjajuci parametar t
std::vector<glm::vec3> tanB; 
std::vector<glm::vec3> dtanB;

glm::vec3 s(0.0f, 0.0f, 1.0f);
glm::vec3 e(0.0f, 0.0f, 0.0f);

bool pause = false; 

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void crtajKordOsi();
void redisplay_all(void);
void myIdle();

bool ucitajBspline(char* spline_file);

glm::vec3 pozicija(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4);
glm::vec3 tangenta(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4);
glm::vec3 derivacija2(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4);
//*************************
//**************************


void redisplay_all(void)
{
	glutSetWindow(window);
	myReshape(width, height);
	glutPostRedisplay();
}

//*********************************************************************************
//	Glavni program.
//*********************************************************************************

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	// postavljanje dvostrukog spremnika za prikaz (zbog titranja)
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);

	window = glutCreateWindow("Tijelo");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glutIdleFunc(myIdle);

	obj_to_draw = loadOBJ(model_path);
	if (!obj_to_draw) {
		std::cout << "Object failed to load" << std::endl;
	}

	ucitajBspline(spline_path);
	//s = tanB[0]; //pocetni smjer je pocetna tangenta krivulje
	obj_to_draw->translate(splineB[0] - obj_to_draw->center); //pomakni u ishodiste
	
	glm::vec3 ws = tanB[0];
	glm::vec3 us = glm::cross(ws, dtanB[0]);
	glm::vec3 vs = glm::cross(ws, us);

	float m[9]; // rotacijska matrica, column-wise
	m[0] = ws.x; m[1] = ws.y; m[2] = ws.z;
	m[3] = us.x; m[4] = us.y; m[5] = us.z;
	m[6] = vs.x; m[7] = vs.y, m[8] = vs.z;

	glm::mat3 rot_mat = glm::make_mat3(m);
	glm::mat3 rot_inv = glm::inverse(rot_mat);
	//obj_to_draw->rotate(rot_inv);

	glutMainLoop();
	return 0;
}



//x-> buf ->[. . .]
bool ucitajBspline(char* spline_file) {
	std::vector<glm::vec3> bs_points;
	bool success = readFile(bs_points, spline_file);

	if (!success) {
		std::cout << "Datoteka neuspjesno procitana" << std::endl;
		return false;
	}

	brB = bs_points.size();
	//broj segmenata
	brS = brB - 3;
	std::cout << "Ucitano " << brB << " tocaka za B-spline." << std::endl;

	for (glm::vec3 p : bs_points){
		std::cout << p.x << " "; 
		std::cout << p.y << " ";
		std::cout << p.z << " ";
	}

	//std::vector<glm::vec3> p_points;
	//std::vector<glm::vec3> tan_b;
	//std::vector<glm::vec3> der2_b;
	for (int i = 0; i < brS; i += 1) {
		glm::vec3 point1 = bs_points[i];
		glm::vec3 point2 = bs_points[i+1];
		glm::vec3 point3 = bs_points[i+2];
		glm::vec3 point4 = bs_points[i+3];

		float t = 0.0f;
		glm::vec3 poft(0.0f);
		glm::vec3 tanoft(0.0f);
		glm::vec3 der2oft(0.0f);

		//povecavanje t od 0 do 1
		for (int k = 0; k < 100; k++){
			t = k / 100.0f; 
			poft = pozicija(t, point1, point2, point3, point4);
			tanoft = tangenta(t, point1, point2, point3, point4);
			der2oft = derivacija2(t, point1, point2, point3, point4); 
			splineB.push_back(poft);
			tanB.push_back(tanoft);
			dtanB.push_back(der2oft);
		}
		
	}

}

/*
	vra�a poziciju za t, prvu i drugu derivaciju
*/
glm::vec3 pozicija(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4){
	//treba zbrojiti poziciju od zadnje
	float f1 = (-1 * pow(t, 3.0) + 3 * pow(t, 2.0) - 3 * t + 1) / 6.0f;
	float f2 = (3 * pow(t, 3.0) + -6 * pow(t, 2.0) + 4 ) / 6.0f;
	float f3 = (-3 * pow(t, 3.0) + 3 * pow(t, 2.0) + 3 * t + 1 ) / 6.0f;
	float f4 = pow(t, 3.0) / 6.0f ;

	glm::vec3 p(0.0f);  
	p.x = f1 * r1.x + f2 * r2.x + f3 * r3.x + f4 * r4.x; 
	p.y = f1 * r1.y + f2 * r2.y + f3 * r3.y + f4 * r4.y;
	p.z = f1 * r1.z + f2 * r2.z + f3 * r3.z + f4 * r4.z;
	
	return p + p_acc;
}

glm::vec3 tangenta(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4) {
	float df1 = 0.5 * (-pow(t, 2.0) + 2 * t - 1);
	float df2 = 0.5 * (3 * pow(t, 2.0) - 4 * t);
	float df3 = 0.5 * (-3 * pow(t, 2.0) + 2 * t + 1);
	float df4 = 0.5 * (pow(t, 2.0));

	glm::vec3 dft(0.0f);
	dft.x = df1 * r1.x + df2 * r2.x + df3 * r3.x + df4 * r4.x;
	dft.y = df1 * r1.y + df2 * r2.y + df3 * r3.y + df4 * r4.y;
	dft.z = df1 * r1.z + df2 * r2.z + df3 * r3.z + df4 * r4.z;

	float norm = sqrt(pow(dft.x, 2) + pow(dft.y, 2.0) + pow(dft.z, 2.0));

	return dft / norm;
}

glm::vec3 derivacija2(float t, glm::vec3 r1, glm::vec3 r2, glm::vec3 r3, glm::vec3 r4) {

	float ds1 = -t + 1; 
	float ds2 = 3 * t - 2; 
	float ds3 = -3 * t + 1; 
	float ds4 = t;

	glm::vec3 dst(0.0f);
	dst.x = ds1 * r1.x + ds2 * r2.x + ds3 * r3.x + ds4 * r4.x;
	dst.y = ds1 * r1.y + ds2 * r2.y + ds3 * r3.y + ds4 * r4.y;
	dst.z = ds1 * r1.z + ds2 * r2.z + ds3 * r3.z + ds4 * r4.z;

	float norm = sqrt(pow(dst.x, 2) + pow(dst.y, 2.0) + pow(dst.z, 2.0));

	return dst;
}


int k = 0; 
glm::vec3 u(0.0f), v(0.0f), w(0.0f);

void myDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	crtajKordOsi();

	if (pause) return;

	//crtanje spline-a 
	glBegin(GL_LINE_STRIP);
	for (glm::vec3 p : splineB){
		glVertex3f(p.x, p.y, p.z);
	}
	glEnd();
	//crtanje tangente
	glBegin(GL_LINES);
	for (int j = 0; j < 100 * brS; j += 25){
		glColor3f(0.8f, 0.0f, 1.0f);
		glVertex3f(splineB[j].x, splineB[j].y, splineB[j].z);
		glVertex3f(splineB[j].x + 0.1f*tanB[j].x, splineB[j].y + 0.1f*tanB[j].y, splineB[j].z + 0.1f * tanB[j].z);
	}
	glEnd();

	//konacna orijentacija
	e.x = tanB[k].x ;
	e.y = tanB[k].y ;
	e.z = tanB[k].z ;

	glm::vec3 os = glm::cross(s, e);

	float se = glm::dot(s, e); //skalarno mnozenje 
	float aps_s = sqrt(pow(s.x, 2) + pow(s.y, 2) + pow(s.z, 2));
	float aps_e = sqrt(pow(e.x, 2) + pow(e.y, 2) + pow(e.z, 2));

	float fi_rad = acos(se / (aps_s * aps_e));
	//if (se < 0) { fi_rad =  }
	float fi_stup = (fi_rad / (2 * N_PI)) * 360.0f;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); 
	glLoadIdentity();

	
	glTranslatef(splineB[k].x, splineB[k].y, splineB[k].z);
	glRotatef(fi_stup, os.x, os.y, os.z);
	glTranslatef(-obj_to_draw->center.x, -obj_to_draw->center.y, -obj_to_draw->center.z);
	//glTranslatef(-splineB[0].x - splineB[k].x, splineB[0].y - splineB[k].y, splineB[0].z - splineB[k].z);
	
	obj_to_draw->draw();

	glPointSize(5); 
	glBegin(GL_POINTS); 
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(obj_to_draw->center.x, obj_to_draw->center.y, obj_to_draw->center.z); 
	glEnd(); 
	glPopMatrix(); 

	//tangenta w
	w = tanB[k];

	//normala u = p' x p"
	u = glm::cross(w, dtanB[k]);

	//binormala v = w x u
	v = glm::cross(w, u);

	float m[9]; // rotacijska matrica, column-wise
	m[0] = w.x; m[1] = w.y; m[2] = w.z; 
	m[3] = u.x; m[4] = u.y; m[5] = u.z; 
	m[6] = v.x; m[7] = v.y, m[8] = v.z;

	//DCM matrica rotacije
	glm::mat3 rot_mat = glm::make_mat3(m);
	glm::mat3 rot_inv = glm::inverse(rot_mat);

	//crtanje lokalni kordinatni sustav
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(splineB[k].x, splineB[k].y, splineB[k].z);
	glVertex3f(splineB[k].x + 0.2*w.x, splineB[k].y + 0.2*w.y, splineB[k].z + 0.2*w.z);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(splineB[k].x, splineB[k].y, splineB[k].z);
	glVertex3f(splineB[k].x + 0.2*u.x, splineB[k].y + 0.2*u.y, splineB[k].z + 0.2*u.z);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(splineB[k].x, splineB[k].y, splineB[k].z);
	glVertex3f(splineB[k].x + 0.2*v.x, splineB[k].y + 0.2*v.y, splineB[k].z + 0.2*v.z);
	glEnd();

	
	
	//rotacija objekta
	//obj_to_draw->translate(splineB[k]);
	//obj_to_draw->rotate(rot_inv);
	

	k++;
	if (k == 100 * brS - 1) k = 0;

	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

void crtajKordOsi()
{
	//	glutWireCube (1.0);
	//  glutSolidCube (1.0);
	// glutWireTeapot (1.0);
	//glutSolidTeapot (1.0);

	/*
	glBegin(GL_TRIANGLES); // ili glBegin (GL_LINE_LOOP); za zicnu formu
	glColor3ub(255, 0, 0);	glVertex3f(-1.0, 0.0, 0.0);
	glColor3ub(0, 0, 0);	glVertex3f(0.0, 1.0, 0.0);
	glColor3ub(100, 0, 0);	glVertex3f(0.0, 0.0, 1.0);
	glEnd();
	*/

	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 1.0f);
	glEnd();
	
}

void myReshape(int w, int h)
{
	width = w; height = h;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity();
	
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
	glClear(GL_COLOR_BUFFER_BIT);				     // brisanje zaslona
	gluPerspective(45.0, (float)width / height, 0.5, 20.0); // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
	gluLookAt(ociste.x, ociste.y, ociste.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	// ociste x,y,z; glediste x,y,z; up vektor x,y,z
	glColor3ub(0, 0, 255);
	glMatrixMode(GL_MODELVIEW);         // aktivirana matrica modela
}


//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	//	Desna tipka - brise canvas. 
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		ociste.x = 0;
		redisplay_all();
	}
}

//*********************************************************************************
//	Tastatura tipke - esc - izlazi iz programa.
//*********************************************************************************

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey)
	{
	case 'd': ociste.x = ociste.x + 0.1;
		break;

	case 'a': ociste.x = ociste.x - 0.1;
		break;

	case 'w': ociste.y = ociste.y + 0.1;
		break;

	case 's': ociste.y = ociste.y - 0.1;
		break;

	case 'e': ociste.z += 0.1; 
		break; 

	case 'q': ociste.z -= 0.1; 
		break;

	case 'r': ociste.x = 0.0;
		break;

	//space
	case 32: pause = !pause; 
		break;

	case 27:  exit(0);
		break;
	
	default: 
		break;
	}
	redisplay_all();
}



int currentTime = 0; int previousTime = 0;

void myIdle() {
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	int timeInterval = currentTime - previousTime;
	//printf("%d\n", timeInterval);
	if (timeInterval > 10) {
		myDisplay();
		previousTime = currentTime;
	}
}