#include <vector>
#include <glm/glm.hpp>

#ifndef _MY_PATH_H
#define _MY_PATH_H

struct Face{
	unsigned int a;
	unsigned int b;
	unsigned int c;

	Face(unsigned int a, unsigned int b, unsigned int c) {
		this->a = a; this->b = b; this->c = c;
	}
};

class DrawableObject{

public:
	DrawableObject(std::vector<glm::vec3> vert, std::vector<Face> fc);
	void draw();
	void rotate(glm::mat3 rot); 
	void translate(glm::vec3 v);
	glm::vec3 center;

private:
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> vertices_original;
	std::vector<Face> faces;


};


extern DrawableObject* loadOBJ(const char * path);
extern bool readFile(std::vector<glm::vec3>& points, const char* filename);

#endif