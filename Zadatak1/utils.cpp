#include "obj_loader.h"
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <glm/glm.hpp>
#include <GL/glut.h>
#include <fstream>
using namespace std;



std::vector< unsigned int > vertexIndices;

DrawableObject* loadOBJ(const char* path) {
	std::vector <glm::vec3> vertices; 
	std::vector <Face> faces;
	int n_vertices = 0; 
	glm::vec3 obj_center(0.0f); 

	FILE * file = fopen(path, "r");
	if (file == NULL){
		printf("Impossible to open the file !\n");
		return NULL;
	}

	while (1) {
		char lineHeader[128];

		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break;

		if (strcmp(lineHeader, "v") == 0){
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			
			vertices.push_back(vertex);
			n_vertices++;

			obj_center.x += vertex.x; 
			obj_center.y += vertex.y; 
			obj_center.z += vertex.z; 
			
		}
		else if (strcmp(lineHeader, "f") == 0) {
			unsigned int vertexIndex[3];
			int noOfvertices = fscanf(file, "%d %d %d\n", &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);
			if (noOfvertices != 3) {
				cout << "Wrong number of vertices in face" << endl;
				return NULL;
			}
			
			Face f = Face(vertexIndex[0], vertexIndex[1], vertexIndex[2]);
			faces.push_back(f);
		}
	}

	DrawableObject* myobj = new DrawableObject(vertices, faces);
	obj_center.x /= n_vertices;
	obj_center.y /= n_vertices;
	obj_center.z /= n_vertices;
	myobj->center = glm::vec3(obj_center);
	
	return myobj;
	
}

DrawableObject::DrawableObject(std::vector<glm::vec3> vert, std::vector<Face> fc) {
	vertices = vert;

	for (int i = 0; i < vert.size(); i++) {
		vertices_original.push_back(vert[i]);
	}
	faces = fc;
}

void DrawableObject::rotate(glm::mat3 rot) {

	for (int i = 0; i < this->vertices.size(); i++) {
		this->vertices[i] -= this->center;
		this->vertices[i] = this->vertices_original[i] * rot;
		this->vertices[i] += this->center;
	}
	this->center = this->center * rot; 
}

void DrawableObject::translate(glm::vec3 v) {
	for (int i = 0; i < this->vertices.size(); i++) {
		this->vertices[i] = this->vertices_original[i] + v;
	}
	this->center = this->center + v; 
}

void DrawableObject::draw() {
	for (unsigned int i = 0; i < this->vertices.size() - 2; i+=3) {
		glm::vec3 v1 = this->vertices[i];
		glm::vec3 v2 = this->vertices[i+1];
		glm::vec3 v3 = this->vertices[i+2];

		glBegin(GL_LINE_LOOP);
		glColor3f(0.0, 0.0, 1.0f);
			glVertex3f(v1.x, v1.y, v1.z);
			glVertex3f(v2.x, v2.y, v2.z);
			glVertex3f(v3.x, v3.y, v3.z);
		glEnd();
	}
}

bool readFile(std::vector<glm::vec3>& points, const char* filename)
{
	ifstream fs;
	fs.open(filename, std::ios::in);
	

	if (!fs) {
		cout << endl << "Failed to open file " << filename;
		return false;
	}

	float n = 0.0;
	std::vector<float> nums;

	while (!fs.eof()) {
		fs >> n;
		nums.push_back(n);
	}

	for (int i = 0; i < nums.size() - 2; i+=3) {
		points.push_back(glm::vec3(nums[i], nums[i + 1], nums[i + 2]));
	}

	return true;
}