
#ifdef _WIN32
#include <windows.h>             
#include <Windef.h>
#endif

#include <GL/glew.h>
#include <GL/freeglut.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "ParticleSystem.h"
#include "ParticleEmiter.h"
#include "VertexShaders.h"
#include "FragmentShaders.h"
#include "Utils.hpp"


#include <iostream>
#include <stdlib.h>
#include <stdio.h>

//maths
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>




#define N_PARTICLES 1000

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

GLuint window;
GLuint width = 800, height = 600;
float angle;
float oldMouseX;
float oldMouseY;
bool pause = false;

//*********************************************************************************
//	Postavke inicijalne kamere
//*********************************************************************************

glm::vec3 eye = glm::vec3(0.0f, 0.0f, 15.0f);		//ociste
glm::vec3 target = glm::vec3(0.0f, 0.0f, 0.0f);		//glediste
glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);			//up vektor

//*********************************************************************************
//	GLUT function prototypes.
//*********************************************************************************

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void redisplay_all(void);
void myIdle();

//*********************************************************************************
//	Function prototypes
//*********************************************************************************
void addParticleSystem(ParticleSystem& ps);
void crtajKordOsi();
bool init_data();

//**********************************************************************************
// SHADY vars
//**********************************************************************************
unsigned int vertexShader;
unsigned int fragmentShader;
unsigned int ourShader;

//**********************************************************************************
// Teksture 
//**********************************************************************************
std::string tex_path = "C:\\Projects\\IRG\\RAG_labosi\\RG2018\\Particles\\tekstura.bmp";
unsigned int textureID;

//**********************************************************************************
// Global vars
//**********************************************************************************
unsigned int brojac = 0;
unsigned char* texture_data = NULL;
int tex_width;
int tex_height;
int nchannels;

//**********************************************************************************
// Emiters & Particle Systems
//**********************************************************************************
std::vector<ParticleSystem> particleSystems;
ParticleEmiter* pEmiter_1 = NULL;
ParticleEmiter* pEmiter_2 = NULL;
ParticleEmiter* pEmiter_3 = NULL;
ParticleEmiter* pEmiter_4 = NULL;
ParticleSystem* ps_1 = NULL;
ParticleSystem* ps_2 = NULL;
ParticleSystem* ps_3 = NULL;
ParticleSystem* ps_4 = NULL;



int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3); //support OpenGL (>=3.2)
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);

	window = glutCreateWindow("�estice");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	//glutMouseFunc(myMouse);
	//glutKeyboardFunc(myKeyboard);
	//glutIdleFunc(myIdle);
	
	glewExperimental = true;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	init_data();
	
	glutMainLoop();

	return 0;
}

bool init_data() {
	pEmiter_1 = new ParticleEmiter(glm::vec3(3.0f, 0.0f, 0.0f)			// izvor
									, glm::vec3(0.0f, 0.0f, 0.0f)		// pocetna brzina, dodaje se nasumicni vektor
									, glm::vec3(0.0f, -90.81f, 0.0f)	// akceleracija
									, glm::vec3(1.0f, 0.0f, 0.0f)		// boja koja se mnozi s teksturom
	);

	pEmiter_2 = new ParticleEmiter(glm::vec3(0.0f, 0.0f, 0.0f)			// izvor
									, glm::vec3(0.0f, 3.0f, 0.0f)		// pocetna brzina, dodaje se nasumicni vektor
									, glm::vec3(0.0f, -90.81f, 0.0f)	// akceleracija
									, glm::vec3(1.0f, 1.0f, 1.0f)		// boja koja se mnozi s teksturom
	);
	

	ps_1 = new ParticleSystem(
		pEmiter_1					// pokazivac na emiter objekt
		, 1000				// broj cestica
		, eye						// ociste
	);
	addParticleSystem(*ps_1);

	ps_2 = new ParticleSystem(
		pEmiter_2					// pokazivac na emiter objekt
		, 1000				// broj cestica
		, eye						// ociste
	);
	addParticleSystem(*ps_2);

	// #1 bind VAO first !!
	// #2 bind VBO-s
	// #3 configure Vertex Attributes

	try {
		ps_1->Initialize();	    
	}
	catch (const std::exception&) {
		//do cleanup
		throw std::exception();
	}


	//ucitaj teksturu
	glGenTextures(1, &textureID);
	stbi_loadTexture(textureID, tex_path, texture_data, &tex_width, &tex_height, &nchannels);


	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	checkShaderCompileStatus(vertexShader);

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	checkShaderCompileStatus(fragmentShader);

	ourShader = glCreateProgram();
	glAttachShader(ourShader, vertexShader);
	glAttachShader(ourShader, fragmentShader);
	glLinkProgram(ourShader);
	checkShaderCompileStatus(ourShader);
	checkShaderLinkStatus(ourShader);

	glUseProgram(ourShader);
	glUniform1i(glGetUniformLocation(ourShader, "ourTexture"), 0);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

}


void myDisplay(void)
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//crtajKordOsi();

	glm::mat4 proj = glm::perspective<GLfloat>(
		glm::radians(45.0f),
		width / height,
		0.1f,
		100.0f
	);
	glm::mat4 view = glm::lookAt(eye, target, up);
	glm::mat4 model = glm::mat4(1.0f);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);

	glUseProgram(ourShader);
	GLint proj_loc = glGetUniformLocation(ourShader, "u_proj");
	GLint view_loc = glGetUniformLocation(ourShader, "u_view");
	GLint model_loc = glGetUniformLocation(ourShader, "u_model");
	GLint tex_uniform0 = glGetUniformLocation(ourShader, "ourTexture");

	glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(model));
	glUniform1i(tex_uniform0, 0);


	//Bufferi se bindaju u particle systemu
	for (auto ps : particleSystems) {
		ps.Render();
	}
	//Unbind buffers
	

	if (brojac % 100 == 0) {
		std::cout << brojac << std::endl;
	}
	brojac++;

	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
	glutPostRedisplay();
}

void crtajKordOsi()
{
	
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 1.0f);
	glEnd();

}

void addParticleSystem(ParticleSystem& ps) {
	particleSystems.push_back(ps);
}


void myReshape(int w, int h)
{

	glutPostRedisplay();
}


//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	//	Desna tipka - brise canvas. 
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		eye.x = 0;
		//redisplay_all();
	}
}

//*********************************************************************************
//	Tastatura tipke - esc - izlazi iz programa.
//*********************************************************************************

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey)
	{
	case 'd': eye.x = eye.x + 0.1;
		break;

	case 'a': eye.x = eye.x - 0.1;
		break;

	case 'w': eye.y = eye.y + 0.1;
		break;

	case 's': eye.y = eye.y - 0.1;
		break;

	case 'e': eye.z += 0.1;
		break;

	case 'q': eye.z -= 0.1;
		break;

	case 'r': eye.x = 0.0;
		break;

		//space
	case 32: pause = !pause;
		break;

	case 27:  exit(0);
		break;

	default:
		break;
	}
	//redisplay_all();
}
