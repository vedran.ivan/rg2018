#include "Particle.h"
#include <stdlib.h> //malloc

#include<vector>

void Particle::update(float fTimeStep) {
	p_age += fTimeStep;

	p_velocity += p_acceleration;

	p_position = p_init_position
		+ p_init_velocity * p_age
		+ 0.5f * p_init_acc* glm::pow(p_age, 2);

	return;
}

void Particle::render() {
	//TODO
	return; 
}

void Particle::run() {
	//this->update(); 
	this->render(); 
}

bool Particle::isDead() {
	if (p_age >= p_lifeSpan) {
		return true;
	}
	return false;
}
