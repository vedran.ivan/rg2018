#ifndef PARTICLE_H
#define PARTICLE_H

#include <glm/glm.hpp>

#include <GL/freeglut.h>
#include <vector>
#include <memory>

struct Particle
{
	Particle()
		: p_init_position(glm::vec3(0.0f))
		, p_init_velocity(glm::vec3(0.0f))
		, p_init_acc(glm::vec3(0.0f))
		, p_Force(glm::vec3(0.0f))
		, p_position(p_init_position)
		, p_velocity(p_init_velocity)
		, p_acceleration(p_init_acc)
		, p_size(0.0f)
		, p_color(glm::vec3(0.0f, 0.0f , 0.0f))
		, p_age(0.0f)
		, p_lifeSpan(1.0f)
		, p_isAlive(false)
	{};

public:
	glm::vec3   p_init_position; 
	glm::vec3   p_init_velocity; 
	glm::vec3   p_init_acc; 
	glm::vec3   p_Force;

	glm::vec3   p_position; 
	glm::vec3   p_velocity; 
	glm::vec3   p_acceleration;
	glm::vec3	p_color;
	GLfloat     p_size;    
	GLfloat     p_age; 
	GLfloat     p_lifeSpan;
	bool		p_isAlive;

	void run();
	bool isDead();
	void update(float timeStep);

private: 
	void render(); 

};
typedef std::vector<std::unique_ptr<Particle>> ParticleBuffer;

#endif