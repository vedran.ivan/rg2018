
#include "ParticleEmiter.h"
#include "Random.h"
#include <iostream>

ParticleEmiter::ParticleEmiter(glm::vec3 source, glm::vec3 ivelocity, glm::vec3 iacc, glm::vec3 color) {
	e_source = source;
	e_velocity = ivelocity;
	e_acc = iacc;
	e_color = color;
}

void ParticleEmiter::EmitParticle(Particle& particle) {
	
	glm::vec3 runivec = random_uniform();

		
	particle.p_age = 0.0f;
	particle.p_lifeSpan = RandRange(0, 10);
	particle.p_color = e_color;


	particle.p_init_position = e_source;
	particle.p_init_velocity = (e_velocity + runivec )* RandRange(10, 15);
	particle.p_velocity = particle.p_init_velocity;
	particle.p_init_acc = e_acc;
	particle.p_acceleration = particle.p_init_acc;


	return;
}