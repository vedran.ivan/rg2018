#ifndef PARTICLE_EMITER_H 
#define PARTICLE_EMITER_H

#include "Particle.h"

class ParticleEmiter {

public:
	ParticleEmiter(glm::vec3 source, glm::vec3 ivelocity, glm::vec3 iacc, glm::vec3 color) ;

	void EmitParticle(Particle& particle);

private:
	glm::vec3 e_source; 
	glm::vec3 e_acc;
	glm::vec3 e_velocity;

	glm::vec3 e_color;
};


#endif // !PARTICLE_EMITER_H 

