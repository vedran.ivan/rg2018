#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

#include "Utils.hpp" //glew
#include "Particle.h"
#include "ParticleEmiter.h"
#include "ParticleSystem.h"

#include "Random.h"

#include <GL/freeglut.h>

#include <glm/glm.hpp>

//#include "SOIL.h"

#ifdef LINUX_UBUNTU_SEGFAULT
#include <pthread.h>
#endif

class ParticleEmiter;



ParticleSystem::ParticleSystem
(
	ParticleEmiter* pemiter
	, unsigned int npart
	, glm::vec3 _camera )

	: ps_ParticleEmiter(pemiter)
	, ps_ParticleBuffer(NULL)
	, n_particles(npart)
	, VertexBuffer(NULL)
	, m_camera(_camera)
	, pTime_int(0.005f)
{
	VertexBuffer = new float[npart * 32];
	//delete[] na kraju

	Initialize();
	

};//resize(numParticles)

ParticleSystem::~ParticleSystem() {
	if (ps_TextureID != 0)
	{
		glDeleteTextures(1, &ps_TextureID);
		ps_TextureID = 0;
	}
}

void ParticleSystem::setParticleEmitter(ParticleEmiter* pemiter) {
	this->ps_ParticleEmiter = pemiter;
}

void ParticleSystem::GenBuffers() {
	glGenVertexArrays(1, &m_VAO_ID);
	glGenBuffers(1, &m_VBO_ID); //particle system specific
	glGenBuffers(1, &m_EBO_ID);
}

void ParticleSystem::BindBuffers() {
	glBindVertexArray(this->m_VAO_ID);
	glBindBuffer(GL_ARRAY_BUFFER, this->m_VBO_ID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->m_EBO_ID);
}

void ParticleSystem::UnbindBuffers() {
	glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind VBO
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); //EBO se ne smije prije VBO unbindati
	glBindVertexArray(0); //unbind VAO
}


void ParticleSystem::Initialize() {
	CreateParticles(); 
	InitializeParticles(); 


	GenBuffers();
	BindBuffers();

	unsigned int n_indices = n_particles * 6;
	VertexIndices = new unsigned int[n_indices];
	

	int k = 0;
	for (int i = 0; i < n_particles * 6; i += 6) {
		VertexIndices[i] = k;
		VertexIndices[i + 1] = k + 1;
		VertexIndices[i + 2] = k + 2;

		VertexIndices[i + 3] = k;
		VertexIndices[i + 4] = k + 2;
		VertexIndices[i + 5] = k + 3;

		k += 4;
	}


	if (VertexBuffer != NULL && VertexIndices != NULL) {
		glBufferData(GL_ARRAY_BUFFER, n_particles * 32 * sizeof(float), VertexBuffer, GL_DYNAMIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, n_indices * sizeof(VertexIndices[0]), VertexIndices, GL_STATIC_DRAW);
		
	} 
	else
	{
		std::cerr << "ERROR::ParticleSystem::Initialize::VBO OR EBO BUFFER IS NULL";
		throw std::exception();
	}
	
	glEnableVertexAttribArray(0); //position
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(void*)(0)
	);
	glEnableVertexAttribArray(1); //color
	glVertexAttribPointer(1,
		3,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(void*)(3 * sizeof(GLfloat))
	);
	glEnableVertexAttribArray(2); //texture
	glVertexAttribPointer(2,
		2,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(void*)(6 * sizeof(GLfloat))
	);


	UnbindBuffers();
	//delete[] indices;
	//indices = NULL;
	
}

void ParticleSystem::CreateParticles() {

	ps_ParticleBuffer = new ParticleBuffer();

	for (GLuint i = 0; i < n_particles; ++i) {
		std::unique_ptr<Particle> up(new Particle());
		ps_ParticleBuffer->push_back(std::move(up));
	}

}

void ParticleSystem::InitializeParticles() {

	for (unsigned int i = 0; i < n_particles; ++i) {
		auto &uq_ptr = ps_ParticleBuffer->at(i);
		Particle* p = uq_ptr.get();
		if (ps_ParticleEmiter != NULL) {
			EmitParticle(uq_ptr);
			ps_ParticleEmiter->EmitParticle(*p);
		}
		else {
			RandomizeParticle(p);
		}
	}

}

void ParticleSystem::Update(float fTimeStep) {
	
	for (unsigned int i = 0; i < n_particles; ++i) {
		auto &p = ps_ParticleBuffer->at(i); 
		if (p.get()->isDead()) {
			if (ps_ParticleEmiter != NULL) {
				//EmitParticle(p);
				ps_ParticleEmiter->EmitParticle(*p.get());
			} 
			else {
				RandomizeParticle(p.get());
			}
		}
		p.get()->update(fTimeStep);
	}

	return;
}

void ParticleSystem::makeVertexBuffer() {
	//consider particle.size/2
	glm::vec4 X(0.05f, 0.0f, 0.0f, 0.0f);
	glm::vec4 Y(0.0f, 0.05f, 0.0f, 0.0f);

	//pocetna normala na povrsinu, uvijek u smjeru -z osi
	glm::vec3 s(0.0f, 0.0f, -1.0f);

	unsigned int stride = 8; //razmak izmedu pojedinih vertexa
	unsigned int bsize = 4 * stride; //razmak izmedu particle-a

	std::vector <glm::vec2> texcoords;
	glm::vec2 tc1 = glm::vec2(0.0f, 0.0f);
	glm::vec2 tc2 = glm::vec2(0.0f, 1.0f);
	glm::vec2 tc3 = glm::vec2(1.0f, 1.0f);
	glm::vec2 tc4 = glm::vec2(1.0f, 0.0f);
	texcoords.push_back(tc1);
	texcoords.push_back(tc2);
	texcoords.push_back(tc3);
	texcoords.push_back(tc4);

	float rc;

	for (unsigned int i = 0; i < n_particles; ++i) {
		auto &uq_p = ps_ParticleBuffer->at(i);
		Particle* p = uq_p.get();

		glm::vec3 e(p->p_position.x - m_camera.x, p->p_position.y - m_camera.y, p->p_position.z - m_camera.z);
		glm::vec3 os = glm::normalize(glm::cross(s, e));

		GLfloat fi = getRotationDegreeAngle(s, e);
		glm::mat4 R = getRotationMatrix(fi, os);
		
		std::vector <glm::vec3> corners;
		
		glm::vec3 tmp1 = p->p_position + glm::vec3(R * ((-X - Y)));  // donji lijevi
		glm::vec3 tmp2 = p->p_position + glm::vec3(R * ((-X + Y)));  // gornji lijevi
		glm::vec3 tmp3 = p->p_position + glm::vec3(R * ((X + Y)));   // gornji desni
		glm::vec3 tmp4 = p->p_position + glm::vec3(R * ((X - Y)));   // donji desni
		corners.push_back(tmp1); 
		corners.push_back(tmp2); 
		corners.push_back(tmp3); 
		corners.push_back(tmp4); 

		

		for (int j = 0; j < 4; j++) {

			//pozicije
			VertexBuffer[bsize*i + j * stride + 0] = corners[j].x; 
			VertexBuffer[bsize*i + j * stride + 1] = corners[j].y;
			VertexBuffer[bsize*i + j * stride + 2] = corners[j].z;

			//boja
			VertexBuffer[bsize*i + j * stride + 3] = p->p_color.x;  //1.0 * ((float)rand() / (RAND_MAX));
			VertexBuffer[bsize*i + j * stride + 4] = p->p_color.y;	//1.0 * ((float)rand() / (RAND_MAX));
			VertexBuffer[bsize*i + j * stride + 5] = p->p_color.z;  //1.0 * ((float)rand() / (RAND_MAX));

			//tekstura
			VertexBuffer[bsize*i + j * stride + 6] = texcoords[j].x;
			VertexBuffer[bsize*i + j * stride + 7] = texcoords[j].y;
		}


	}


}


void ParticleSystem::Render() {
	
	Update(pTime_int);
	makeVertexBuffer();

	BindBuffers();
	
	glBufferData(GL_ARRAY_BUFFER, n_particles * 32 * sizeof(float), VertexBuffer, GL_DYNAMIC_DRAW);

	glDrawElements(GL_TRIANGLES, n_particles * 2, GL_UNSIGNED_INT, 0); //crtaj trokute, 0 mora biti na zadnjem mjestu


	UnbindBuffers(); 


	return;
}

void ParticleSystem::RandomizeParticle(Particle* particle) {
	//std::unique_ptr<Particle> myparticle(new Particle()); 

	glm::vec3 runivec = random_uniform();

	particle->p_age = 0.0f;
	particle->p_lifeSpan = RandRange(0, 10);

	particle->p_init_position = runivec * 1.0f;
	particle->p_init_velocity = runivec * RandRange(10, 15);
	particle->p_init_acc = glm::vec3(0.0f, -90.81f, 0.0f);
	particle->p_acceleration = particle->p_init_acc;

	return;
}


void ParticleSystem::EmitParticle(std::unique_ptr<Particle>& pp) {
	assert(ps_ParticleEmiter != NULL);
	Particle particle = *pp.get();
	ps_ParticleEmiter->EmitParticle(particle);
}

void ParticleSystem::EmitParticles() {
	for (auto &p: *ps_ParticleBuffer) { //iteriraj po referenci, std_unique_ptr ne moze radit kopije
		EmitParticle(p);
	}
	return;
}

void ParticleSystem::ResizeParticleBuffer(GLuint n_particles) {
	return;
}
