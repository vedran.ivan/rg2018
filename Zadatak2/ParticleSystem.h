#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

#include "Particle.h"
#include "ParticleEmiter.h"

class ParticleSystem {
public: 
	ParticleSystem(ParticleEmiter* pemiter, unsigned int n_particles, glm::vec3 _camera);
	virtual ~ParticleSystem();

	void setParticleEmitter(ParticleEmiter* pemiter);

	//RandomizeParticles()
	void EmitParticles();

	virtual void Initialize();
	virtual void Update(float fTimeStep);
	virtual void Render(); // aktivira svoj VBO


	void ResizeParticleBuffer(GLuint n_particles);
	void makeVertexBuffer();
	void RandomizeParticle(Particle* particle);
	void InitializeParticles();
	void CreateParticles();

	GLuint n_particles; 

protected:
	void EmitParticle(std::unique_ptr<Particle>& particle);
private:
	void GenBuffers();
	void BindBuffers();
	void UnbindBuffers();


	ParticleEmiter* ps_ParticleEmiter;
	ParticleBuffer* ps_ParticleBuffer;
	float*			VertexBuffer;
	unsigned int*	VertexIndices = NULL;

	GLuint			ps_TextureID; 
	glm::vec3		m_camera;

	float	        pTime_int;

	unsigned int m_VAO_ID;
	unsigned int m_VBO_ID;
	unsigned int m_EBO_ID;

};

#endif // !PARTICLE_SYSTEM_H



