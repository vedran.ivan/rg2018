
#include <memory>
#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/norm.hpp>


struct A {
public:
	A(): x(10) {};
	A(int a) : x(a) {};
	
	void change(int val);
	int getx();

private:
	int x;

};

int A::getx() {
	return x;
}
void A::change(int val) {
	x = val;
}

typedef std::vector<std::unique_ptr<A>> VecBuffer;

int main() {

	std::vector<float> myfloats;
	myfloats.push_back(1);
	myfloats.push_back(2);
	myfloats.push_back(3);
	myfloats.push_back(4);

	for (auto mvar : myfloats) {
		std::cout << mvar << std::endl;
	}
	
	return 0;
}