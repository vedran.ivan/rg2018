#include "Utils.hpp"

#include "stb_image.h"

bool stbi_loadTexture(unsigned int tID,
	std::string &filename,
	unsigned char*& buffer,
	int* t_Width,
	int* t_Height,
	int* nrChannels)
{
	if (FILE *file = fopen(filename.c_str(), "r")) {
		fclose(file);
		std::cout << "File exists" << std::endl;
	}
	else {
		std::cout << "File doesn't exist" << std::endl;
		return false;
	}

	buffer = stbi_load(filename.c_str(), t_Width, t_Height, nrChannels, 0);

	if (buffer)
	{
		glActiveTexture(GL_TEXTURE + tID);
		glBindTexture(GL_TEXTURE_2D, tID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)*t_Width, (GLsizei)*t_Height, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
		stbi_image_free(buffer);

		std::cout << "::STBI::SUCCESS::LOADED TEXTURE" << std::endl;
		std::cout << "width: " << *t_Width << std::endl;
		std::cout << "height: " << *t_Height << std::endl;

		return true;
	}
	else
	{
		std::cout << "::STBI::ERROR::STBI_LOAD RETURNED NULL" << std::endl;
	}


	return false;
}

void checkShaderLinkStatus(unsigned int ID) {
	int status;
	GLchar infoLog[1024];
	glGetProgramiv(ID, GL_LINK_STATUS, &status);
	if (!status) {
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::" << ID << "::LINKING_FAILURE\n" << infoLog << std::endl;
	}
	else {
		std::cout << "SHADER::" << ID << "::LINKING_SUCCESS" << std::endl;
	}
}

void checkShaderCompileStatus(unsigned int ID) {
	int status;
	GLchar infolog[1024];
	glGetShaderiv(ID, GL_COMPILE_STATUS, &status);
	if (!status) {
		glGetProgramInfoLog(ID, 512, NULL, infolog);
		std::cout << "ERROR::SHADER::" << ID << "::COMPILATION_FAILURE\n" << infolog << std::endl;
	}
	else {
		std::cout << "SHADER::" << ID << "::COMPILATION_SUCCESS" << std::endl;
	}
}
glm::mat4 getRotationMatrix(GLfloat fi,glm::vec3 os) {
	
	glm::mat4 m_rot = glm::rotate(glm::radians(fi), os);
	
	return m_rot;
}

// s = (0,0,1.0)
glm::vec3 getRotationAxis(glm::vec3 s, glm::vec3 e) {
	glm::vec3 os = glm::cross(s, e);
	return os;
}

GLfloat getRotationDegreeAngle(glm::vec3 s, glm::vec3 e) {
	GLfloat se = glm::dot(s, e); //skalarno mnozenje 
	GLfloat aps_s = sqrt(pow(s.x, 2) + pow(s.y, 2) + pow(s.z, 2));
	GLfloat aps_e = sqrt(pow(e.x, 2) + pow(e.y, 2) + pow(e.z, 2));
	
	GLfloat fi_rad = acos(se / (aps_s * aps_e));
	GLfloat fi_stup = (fi_rad / (2 * glm::pi<GLfloat>())) * 360.0f;
	return fi_stup;
}

std::ostream &operator<< (std::ostream &out, const glm::mat4 &mat) {
	int i = 0;
	const float* pt = (const float*)glm::value_ptr(mat);

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			out << std::setw(10) << pt[4 * j + i];
		}
		out << "\n";
	}

	return out;
}

std::ostream &operator<< (std::ostream &out, const glm::vec4 &vec) {
	int i = 0;
	const float* pt = (const float*)glm::value_ptr(vec);

	out << " [ " << pt[0];
	out << std::setw(10) << pt[1];
	out << std::setw(10) << pt[2];
	out << std::setw(10) << pt[3] << " ]\n";

	return out;
}