#ifndef UTILS_HPP
#define UTILS_HPP

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <iomanip>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/norm.hpp>


void checkShaderCompileStatus(unsigned int ID);
void checkShaderLinkStatus(unsigned int ID);
bool stbi_loadTexture(unsigned int tID, std::string &filename,
	unsigned char*& buffer,
	int* t_Width,
	int* t_Height,
	int* nrChannels);
std::ostream &operator<< (std::ostream &out, const glm::vec4 &vec);
std::ostream &operator<< (std::ostream &out, const glm::mat4 &mat);
glm::mat4 getRotationMatrix(GLfloat fi, glm::vec3 os);
GLfloat getRotationDegreeAngle(glm::vec3 s, glm::vec3 e);

#endif